var obj = {
    init: function (options, element) {
        var $this = this;

        this.options = $.extend({}, this.options, options);
        this.element = element;
        this.link = $(this.element).find(this.options.link);
        this.input = $(this.element).find('#' + this.options.inputId);

        $(this.link).on('click', function(e) {
            e.preventDefault();
            $($this.input).val($(this).data('rating'));
            $this._renderStars();
        });

        return this;
    },

    options: {
        activeClass: 'fas',
        inactiveClass: 'far',
        totalStars: 5,
        link: 'a[data-rating]'
    },

    _renderStars: function() {
        var $this = this,
            count = parseInt($($this.input).val());

        $($this.link).each(function(index) {
            var $icon = $(this).children();

            if (count > index) {
                $icon.removeClass($this.options.inactiveClass).addClass($this.options.activeClass);
            } else {
                $icon.removeClass($this.options.activeClass).addClass($this.options.inactiveClass);
            }
        });
    },
};

if ( typeof Object.create !== "function" ) {
    Object.create = function (o) {
        function F() {}
        F.prototype = o;
        return new F();
    };
}
$.plugin = function( name, object ) {
    $.fn[name] = function( options ) {
        return this.each(function() {
            if ( ! $.data( this, name ) ) {
                $.data( this, name, Object.create(object).init(
                    options, this ) );
                }
            });
        };
    };
$.plugin('rating', obj);
