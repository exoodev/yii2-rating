<?php

namespace exoo\rating;

use yii\helpers\Json;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Rating widget
 *
 * For example to use the rating with a [[\yii\base\Model|model]]:
 *
 * ```php
 * echo Rating::widget([
 *     'model' => $model,
 *     'attribute' => 'vote',
 *     'clientOptions' => [],
 * ]);
 * ```
 *
 * The following example will use the name property instead:
 *
 * ```php
 * echo Rating::widget([
 *     'name'  => 'vote',
 *     'value'  => $value,
 * ]);
 * ```
 *
 * You can also use this widget in an [[\yii\widgets\ActiveForm|ActiveForm]] using the [[\yii\widgets\ActiveField::widget()|widget()]]
 * method, for example like this:
 *
 * ```php
 * <?= $form->field($model, 'vote')->widget(Rating::class, [
 *     'selectable' => 'true',
 * ]) ?>
 * ```
 */
class Rating extends InputWidget
{
    /**
     * @var array the options for the underlying JS plugin.
     */
    public $clientOptions = [];
    /**
     * @var integer the total stars
     */
    public $totalStars = 5;
    /**
     * @var string the template
     */
    public $iconOptions = ['class' => 'fa-star fa-fw uk-text-primary'];
    /**
     * @var string the property
     */
    public $starsOptions = ['class' => 'uk-flex uk-flex-inline'];
    /**
     * @var string the property
     */
    public $containerOptions = [];
    /**
     * @var string the property
     */
    public $activeClass = 'fas';
    /**
     * @var string the property
     */
    public $inactiveClass = 'far';
    /**
     * @var boolean
     */
    public $selectable = false;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();
        $id = $this->containerOptions['id'] = $this->options['id'] . '-container';

        if ($this->hasModel()) {
            $this->value = !isset($this->options['value']) 
                ? Html::getAttributeValue($this->model, $this->attribute)
                : $this->options['value'];

            if ($this->selectable) {
                $view = $this->getView();
                $options = Json::htmlEncode(array_merge([
                    'inputId' => $this->options['id'],
                    'totalStars' => $this->totalStars,
                    'activeClass' => $this->activeClass,
                    'inactiveClass' => $this->inactiveClass,
                ], $this->clientOptions));
                $view->registerJs("jQuery('#$id').rating($options)");
                RatingAsset::register($view);
            }
        }
    }

    /**
     * Renders the widget.
     */
    public function run()
    {
        Html::addCssClass($this->starsOptions, 'ex-rating-stars');
        $stars = Html::tag('div', implode(PHP_EOL, $this->getIcons()), $this->starsOptions);

        if ($this->hasModel()) {
            $stars .= Html::activeHiddenInput($this->model, $this->attribute, $this->options);
        }

        Html::addCssClass($this->containerOptions, 'ex-rating-container');
        echo Html::tag('div', $stars, $this->containerOptions);
    }

    protected function getIcons()
    {
        $result = [];

        for ($i=0; $i < $this->totalStars; $i++) {
            Html::removeCssClass($this->iconOptions, [$this->activeClass, $this->inactiveClass]);
            Html::addCssClass($this->iconOptions, $this->value > $i ? $this->activeClass : $this->inactiveClass);
            $icon = Html::tag('i', null, $this->iconOptions);

            if ($this->hasModel() && $this->selectable) {
                $icon = Html::a($icon, '#', ['data-rating' => $i + 1]);
            }

            $result[] = $icon;
        }

        return $result;
    }
}
