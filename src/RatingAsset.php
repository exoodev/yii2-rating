<?php

namespace exoo\rating;

/**
 * Asset bundle for widget [[RatingAsset]].
 */
class RatingAsset extends \yii\web\AssetBundle
{
    /**
     * @inheritdoc
     */
    public $sourcePath = '@exoo/rating/assets';
    /**
     * @inheritdoc
     */
    public $js = [
        'js/rating.js',
    ];
    /**
     * @inheritdoc
     */
    public $depends = [
        'exoo\fontawesome\FontAwesomeAsset',
    ];
}
